%global commit0 f3774bc63bb45ff9daec2fa1ff14911d61fbe8a2
%global pybasever 3.6
%global pylibdir %{_libdir}/python%{pybasever}

Name:       planter
Version:    1.6.1
Release:    1%{?dist}
Summary:    Project directory planter

License:    GPLv3
URL:        http://www.slackermedia.info/planter
Source0:    https://gitlab.com/%{name}/%{name}/repository/archive.tar.gz?ref=%{version}#/%{name}-%{version}-%{commit0}.tar.bz2

BuildRequires:  desktop-file-utils
Requires:       python3-qt5
Requires:       python3-qt5-webkit
Requires:       python3-inotify
Requires:       python3-dbus
Requires:       python3-PyYAML
BuildArch:      noarch

%description
Planter is a project directory generator. It creates a full directory tree for projects from user-defined YAML templates. It has both a shell and GUI interface.

%prep
%autosetup -n %{name}-%{version}-%{commit0}

#%setup -q
#%build
%install
install -Dm 755 usr/share/icons/hicolor/scalable/apps/%{name}.svg %{buildroot}/%{_datadir}/pixmaps/%{name}.svg

mkdir -p %{buildroot}/%{_sysconfdir}/%{name}
install -Dm 755 etc/%{name}/* %{buildroot}/%{_sysconfdir}/%{name}

mkdir -p %{buildroot}%{_mandir}/man8
cat usr/man/man8/%{name}.8 | gzip > %{buildroot}%{_mandir}/man8/%{name}.8.gz

mkdir -p %{buildroot}%{pylibdir}/site-packages/%{name}
install -Dm 755 usr/lib/python3/site-packages/%{name}/*py %{buildroot}%{pylibdir}/site-packages/%{name}

mkdir -p %{buildroot}%{_bindir}
install -m 755 %{name} %{buildroot}%{_bindir}

mkdir -p %{buildroot}/%{_docdir}/%{name}
install -Dm 644 usr/doc/%{name}/COPYING %{buildroot}/%{_docdir}/%{name}
install -Dm 644 usr/doc/%{name}/AUTHORS %{buildroot}/%{_docdir}/%{name}
install -Dm 644 usr/doc/%{name}/README  %{buildroot}/%{_docdir}/%{name}

desktop-file-install \
    --dir %{buildroot}%{_datadir}/applications \
    usr/share/applications/%{name}.desktop

%check

%files
%{_docdir}/%{name}/COPYING
%{_docdir}/%{name}/AUTHORS
%{_docdir}/%{name}/README
%{pylibdir}/site-packages/%{name}
%{_datadir}/pixmaps/%{name}.svg
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_mandir}/man8/%{name}.8.gz
%{_sysconfdir}/%{name}/style.css
%{_sysconfdir}/%{name}/tree.list


%changelog
* Mon Jan 08 2018 Klaatu <klaatu@fedorapeople.org> - 1.6.
- Still fixing paths
* Mon Jan 08 2018 Klaatu <klaatu@fedorapeople.org> - 1.6
- RPM update for F27
- Better conformed to Python filesystem paths
* Mon Jul 03 2017 Klaatu <klaatu@fedorapeople.org> - 1.5.0
- RPM installer
